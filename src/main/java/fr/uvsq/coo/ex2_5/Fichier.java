package fr.uvsq.coo.ex2_5;

public class Fichier {
	private String nom;
	private int taille;
	
	public Fichier(String nom, int taille){
		this.nom = nom;
		this.taille = taille;
	}
	
	public String getNom(){
		return nom;
	}
	public int getTaille(){
		return taille;
	}
	public void setNom(String n){
		this.nom = n;
	}
	public void setTaille(int t){
		this.taille = t;
	}

}
