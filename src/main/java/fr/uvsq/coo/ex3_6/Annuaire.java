package fr.uvsq.coo.ex3_6;

import java.util.Iterator;

public class Annuaire {
	private static Annuaire annuaire;
	private Groupe gpRacine;
	Iterator<Composant> iter;
	
	private Annuaire(){
		gpRacine = new Groupe("Racine");
	}
	
	public static Annuaire getAnnuaire(){
		if(null == annuaire){
			annuaire = new Annuaire();
		}
		
		return annuaire;
	}
	
	public Groupe getGPRacine(){
		return gpRacine;
	}
	
	public void afficher(){
		System.out.println(getGPRacine().getName());
		iter = getGPRacine().getComposant().iterator();
        while(iter.hasNext()) {
		    	 	Composant element = iter.next();
		    	 	element.afficher();
		     	}
	}

}
