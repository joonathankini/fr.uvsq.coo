package fr.uvsq.coo.ex3_6;

import java.util.ArrayList;
import java.util.Iterator;


public class Groupe implements Composant{
	private String gpName = "test";
	private ArrayList<Composant> composant;
	Iterator<Composant> iter;
	
	public Groupe(String name){
		this.gpName = name;
		this.composant = new ArrayList<Composant>();
	}
	
	public void addComposant(Composant c){
		composant.add(c);
	}
	
	public void removeComposant(Composant c){
		composant.remove(c);
	}
	
	public ArrayList<Composant> getComposant(){
		return composant;
	}
	
	public String getName(){
		return gpName;
	}
	
	public void afficher(){
		//System.out.println("heho");
		iter = composant.iterator();
        while(iter.hasNext()) {
		    	 	Composant element = iter.next();
		    	 	System.out.println(element.getName());
		    	 	element.afficher();
		     	}
	}

}
