package fr.uvsq.coo.ex3_6;

import java.time.LocalDate;
import java.util.ArrayList;

import java.time.LocalDate;

public class Main {
	public static void main(String [] args){
		ArrayList<String> tel1 = new ArrayList<String>();
		tel1.add("70785625, 75792431");
		ArrayList<String> tel2 = new ArrayList<String>();
		tel2.add("70785625, 75792431");
		ArrayList<String> tel3 = new ArrayList<String>();
		tel3.add("70785625, 75792431");
		ArrayList<String> tel4 = new ArrayList<String>();
		tel4.add("70785625, 75792431");
		
		Personnel p1 = new Personnel.PersonnelBuilder("KINI", "Jonathan", "Developper", null)
				.Tel(tel1)
				.build();
		Personnel p2 = new Personnel.PersonnelBuilder("Zoungrana", "Brice", "Ingenieur Electricien", null)
				.Tel(tel2)
				.build();
		Personnel p3 = new Personnel.PersonnelBuilder("Ido", "Issouf", "Manager", null)
				.Tel(tel3)
				.build();
		Personnel p4 = new Personnel.PersonnelBuilder("Kabore", "Assami", "Security", null)
				.Tel(tel4)
				.build();
		
		Groupe gp1 = new Groupe("Informatique");
		gp1.addComposant(p1);
		Groupe gp2 = new Groupe("Electricit�");
		gp2.addComposant(p2);
		Groupe gp3 = new Groupe("Marketing");
		gp3.addComposant(p3);
		Groupe gp4 = new Groupe("Securit�");
		gp4.addComposant(p4);
		
		Annuaire annuaire = Annuaire.getAnnuaire();
		Groupe racine = annuaire.getGPRacine();
		racine.addComposant(gp1);
		racine.addComposant(gp2);
		racine.addComposant(gp3);
		racine.addComposant(gp4);
		
		 /*annuaire.getGPRacine().addComposant(gp1);
		 annuaire.getGPRacine().addComposant(gp2);
		 annuaire.getGPRacine().addComposant(gp3);
		 annuaire.getGPRacine().addComposant(gp4);*/
		 
		annuaire.afficher();
		
		//racine.afficher();
		
		
				
				
	}

}
