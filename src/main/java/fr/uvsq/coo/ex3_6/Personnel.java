package fr.uvsq.coo.ex3_6;

import java.time.LocalDate;
import java.util.ArrayList;

public final class Personnel implements Composant{
	private final String Nom;
	private final String Prenom;
	private final LocalDate Naissance;
	private final String Fonction;
	private final ArrayList<String> Tel;
	
	private Personnel(PersonnelBuilder builder){
		this.Nom = builder.Nom;
		this.Prenom = builder.Prenom;
		this.Naissance = builder.Naissance;
		this.Fonction = builder.Fonction;
		this.Tel = builder.Tel;
	}
	
	public String getNom(){
		return Nom;
	}
	public String getFontion(){
		return Fonction;
	}
	public String getPrenom(){
		return Prenom;
	}
	public LocalDate getNaissance(){
		return Naissance;
	}
	public ArrayList<String> getTel(){
		return Tel;
	}
	
	public String getName(){
		return "";
	}
	
	
	public static class PersonnelBuilder{
		private final String Nom;
		private final String Prenom;
		private final String Fonction;
		private final LocalDate Naissance;
		private  ArrayList<String> Tel;
		
		public PersonnelBuilder(String str1,String str2,String str3,LocalDate dt){
			this.Nom = str1;
			this.Prenom = str2;
			this.Fonction = str3;
			this.Naissance = dt;
		}
		
		public PersonnelBuilder Tel(ArrayList<String> l){
			this.Tel=l;
			return this;
		}
		
		public Personnel build(){
			return new Personnel(this);
		}
	}
	
	public void afficher(){
		//System.out.println("heho1");
		String phone= "vide";
		System.out.println(getNom()+" "+getPrenom()+" "+getFontion()+" "+getNaissance());
		System.out.print("Tel:");
		if(this.Tel.size() < 0){
			phone = "RAS";
		}else{
			phone = "";
			for(int i=0;i<this.Tel.size();i++){
				phone+= this.Tel.get(i)+",";
			}
		}
		System.out.println(phone);
	}
}
